package com.vtcproject.mocha.posenet;

public class Constants {
    public static final int REQUEST_CAMERA_PERMISSION = 1;
    public static final int MODEL_WIDTH = 257;
    public static final int MODEL_HEIGHT = 257;
}

