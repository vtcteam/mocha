package com.vtcproject.mocha.posenet

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Rect
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CaptureRequest
import android.hardware.camera2.CaptureResult
import android.hardware.camera2.TotalCaptureResult
import android.media.Image
import android.media.ImageReader
import android.media.ImageReader.OnImageAvailableListener
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.Surface
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.firestore.FirebaseFirestore
import com.vtcproject.mocha.BaseFragment
import com.vtcproject.mocha.R
import com.vtcproject.mocha.models.Room
import com.vtcproject.mocha.posenet.Constants.*
import com.vtcproject.mocha.student.StudentBaseViewModel
import com.vtcproject.mocha.student.ui.student.ReadyRoom
import kotlinx.android.synthetic.main.activity_posenet.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import kotlin.math.abs
import kotlin.math.pow

class PosenetActivity :
        BaseFragment(),
        ActivityCompat.OnRequestPermissionsResultCallback {

    private var noTeam = false


    private val character = arrayOf("e", "i", "l", "o", "t", "u", "v", "x", "y")
    /*
    0 - Nose
    5 - Left Shoulder
    6 - Right Shoulder
    7 - LEFT_ELBOW 肘
    8 - RIGHT_ELBOW 肘
    9 - LEFT_WRIST 手腕
    10 - RIGHT_WRIST 手腕
    11 - LEFT_HIP 臀部
    12 - RIGHT_HIP 臀部
    13 - LEFT_KNEE 膝
    14 - RIGHT_KNEE 膝
    15 - LEFT_ANKLE 腳踝
    16 - RIGHT_ANKLE 腳踝
     */
    private val coordX = arrayOf(arrayOf(59, 131, 164, 208, 150, 183, 65, 132, 167, 194),   //e
            arrayOf(91, 170, 90, 170),                                                      //i
            arrayOf(50, 133, 50, 133, 167, 202),                                            //l
            arrayOf(106, 148, 44, 103, 106, 154, 154, 210),                                 //o
            arrayOf(63, 90, 160, 197, 92, 159),                                             //t
            arrayOf(50, 105, 153, 210, 51, 102, 150, 195),                                  //u
            arrayOf(57, 113, 91, 160, 156, 192),                                            //v
            arrayOf(62, 115, 146, 190, 62, 112, 146, 202),                                  //x
            arrayOf(45, 104, 149, 209, 96, 160)                                             //y
    )
    private val coordY = arrayOf(arrayOf(73, 101, 73, 99, 116, 136, 155, 184, 157, 182),    //e 0
            arrayOf(69, 96, 165, 190),                                                      //i 1
            arrayOf(66, 109, 147, 188, 147, 188),                                           //l 2
            arrayOf(79, 105, 111, 140, 145, 175, 111, 140),                                 //o 3
            arrayOf(83, 112, 80, 110, 156, 181),                                            //t 4
            arrayOf(83, 106, 89, 106, 133, 174, 133, 174),                                  //u 5
            arrayOf(78, 105, 129, 175, 77, 105),                                            //v 6
            arrayOf(74, 94, 72, 91, 154, 172, 153, 172),                                    //x 7
            arrayOf(83, 100, 80, 100, 158, 176)                                             //y 8
    )
    private val countNum = arrayOf(5, 2, 3, 4, 3, 4, 3, 4, 3)

    private val imagedata = arrayOf(R.drawable.e, R.drawable.i, R.drawable.l, R.drawable.o, R.drawable.t, R.drawable.u, R.drawable.v, R.drawable.x, R.drawable.y)
    private var missionTarget: Int = 0  //test i first
    private var missionDone = false
    private var currentPlayer = 0
    private var checkCurrentPlayer = false


    private fun checkImage(person: Person) {
        var i = 0
        var check: Boolean
        var checklist = mutableListOf<Boolean>()
        for (a in 1..countNum.get(missionTarget)) {
            checklist.add(false)
        }
        for (keypoint in person.keyPoints) {
            var coordi = 0
            when (i) {
                0 -> check = true
                in 5..10 -> check = true
                in 13..16 -> check = true
                else -> check = false
            }
            if (check) {
                for (z in 1..countNum.get(missionTarget)) {
                    if (keypoint.position.x > coordX[missionTarget][coordi] &&
                            keypoint.position.x < coordX[missionTarget][coordi + 1] &&
                            keypoint.position.y > coordY[missionTarget][coordi] &&
                            keypoint.position.y < coordY[missionTarget][coordi + 1]) {
                        checklist[z - 1] = true
                    }
                    coordi += 2
                }
            }
            i++
        }
        println(checklist)
        var score = 0
        for (s in 1..checklist.size) {
            if (checklist[s - 1]) {
                score += 1
            }
        }
        if (checklist.size > 3) {
            if (score >= 3) {
                missionDone = true
            }
        }
        if (!checklist.contains(false)) {
            missionDone = true
        }
        println("Mission done: " + missionDone)
        if (missionDone) {
            imgWatermark!!.setImageResource(R.drawable.fullscreen_tick)
            updateTeam()
            endtheGame()
        }
    }

    private fun endtheGame() {
        if (endGame && showoneTime) {
            if (noTeam) {
                score = totalTime / 1000.0
            } else {
                score = mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].totalTime / 1000.0
            }
            Toast.makeText(activity, "Finished", Toast.LENGTH_LONG).show()
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Finish")
            builder.setMessage("Total score is : ${score}\nPlease press ENTER to go next round")
            builder.setPositiveButton("ENTER") { dialog, which ->
                showProgressDialog()
                mViewModel!!.room.uid = mViewModel!!.room.nextRoundRoomUid
                val docRef2 = db.collection("Rooms").document(mViewModel!!.room.uid)
                db.runTransaction { transaction ->
                    val snapshot = transaction.get(docRef2)
                    mViewModel!!.room = snapshot.toObject(Room::class.java)
                    mViewModel!!.student.goNextRound()
                    mViewModel!!.room.updateStudent(mViewModel!!.student)
                    transaction.update(docRef2, "students", mViewModel!!.room.students)
                }.addOnSuccessListener {
                    activity.supportFragmentManager.beginTransaction().replace(R.id.container, ReadyRoom()).commit()
                    hideProgressDialog()
                }


            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
            showoneTime = false
        }

    }

    private var firstTime = true
    private var endGame = false
    private var score = 0.0
    private var showoneTime = true


    private fun updateTeam() {
        if (firstTime) {
            endTime = System.currentTimeMillis()
            totalTime = endTime - startTime
            mViewModel!!.student.endTime = endTime
            mViewModel!!.student.totalTime = totalTime
            mViewModel!!.student.startTime = startTime
            mViewModel!!.student.status = 3
            val docRef = db.collection("Rooms").document(mViewModel!!.room.uid)
            db.runTransaction { transaction ->
                val snapshot = transaction.get(docRef)
                mViewModel!!.room = snapshot.toObject(Room::class.java)
                if (!noTeam) {
                    mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].currentOrder = currentPlayer + 1
                    println(mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].totalTime)
                    mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].totalTime = mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].totalTime + totalTime
                    if (mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder.size == currentPlayer + 1) {
                        mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].isAllDone = true
                        endGame = true
                        showoneTime = true
                        endtheGame()
                    } else {
                        db.collection("Rooms").document(mViewModel!!.room.uid).addSnapshotListener { snapshotforend, e ->
                            Log.e("TAG", "LOG LISTENING")
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e)
                                return@addSnapshotListener
                            }
                            if (snapshotforend != null) {
                                var tmproom = snapshotforend.toObject(Room::class.java)
                                if (tmproom!!.teams[mViewModel!!.student.teamid - 1].isAllDone) {
                                    Log.e("TAG", "LOG LISTENING" + tmproom!!.teams[mViewModel!!.student.teamid - 1].isAllDone)
                                    endGame = true
                                    showoneTime = true
                                    endtheGame()
                                    Log.e("TAG", "LOG LISTENING" + endGame)
                                }

                            } else {
                                Log.d(TAG, "Current data: null")
                            }


                            Log.e("TAG", "LOG LISTENING" + endGame)
                        }
                    }
                }
                mViewModel!!.room.updateStudent(mViewModel!!.student)

                if (noTeam) {
                    endGame = true
                }
                transaction.set(docRef, mViewModel!!.room)
                null
            }.addOnSuccessListener {
                //nothing
            }.addOnFailureListener{e ->
                Log.e("TAG", "Error", e)
            }
        }
        firstTime = false

    }

    private var mViewModel: StudentBaseViewModel? = null

    /** List of body joints that should be connected.    */

    private val bodyJoints = listOf(
            Pair(BodyPart.LEFT_WRIST, BodyPart.LEFT_ELBOW),
            Pair(BodyPart.LEFT_ELBOW, BodyPart.LEFT_SHOULDER),
            Pair(BodyPart.LEFT_SHOULDER, BodyPart.RIGHT_SHOULDER),
            Pair(BodyPart.RIGHT_SHOULDER, BodyPart.RIGHT_ELBOW),
            Pair(BodyPart.RIGHT_ELBOW, BodyPart.RIGHT_WRIST),
            Pair(BodyPart.LEFT_SHOULDER, BodyPart.LEFT_HIP),
            Pair(BodyPart.LEFT_HIP, BodyPart.RIGHT_HIP),
            Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_SHOULDER),
            Pair(BodyPart.LEFT_HIP, BodyPart.LEFT_KNEE),
            Pair(BodyPart.LEFT_KNEE, BodyPart.LEFT_ANKLE),
            Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_KNEE),
            Pair(BodyPart.RIGHT_KNEE, BodyPart.RIGHT_ANKLE)
    )

    /** Threshold for confidence score. */
    private val minConfidence = 0.6

    /** Radius of circle used to draw keypoints.  */
    private val circleRadius = 8.0f

    /** Paint class holds the style and color information to draw geometries,text and bitmaps. */
    private var paint = Paint()

    /** A shape for extracting frame data.   */
    private val PREVIEW_WIDTH = 640
    private val PREVIEW_HEIGHT = 480

    /** An object for the Posenet library.    */
    private lateinit var posenet: Posenet

    /** ID of the current [CameraDevice].   */
    private var cameraId: String? = null

    /** A [SurfaceView] for camera preview.   */
    private var surfaceView: SurfaceView? = null

    private var imgWatermark: ImageView? = null

    /** A [CameraCaptureSession] for camera preview.   */
    private var captureSession: CameraCaptureSession? = null

    /** A reference to the opened [CameraDevice].    */
    private var cameraDevice: CameraDevice? = null

    /** The [android.util.Size] of camera preview.  */
    private var previewSize: Size? = null

    /** The [android.util.Size.getWidth] of camera preview. */
    private var previewWidth = 0

    /** The [android.util.Size.getHeight] of camera preview.  */
    private var previewHeight = 0

    /** A counter to keep count of total frames.  */
    private var frameCounter = 0

    /** An IntArray to save image data in ARGB8888 format  */
    private lateinit var rgbBytes: IntArray

    /** A ByteArray to save image data in YUV format  */
    private var yuvBytes = arrayOfNulls<ByteArray>(3)

    /** An additional thread for running tasks that shouldn't block the UI.   */
    private var backgroundThread: HandlerThread? = null

    /** A [Handler] for running tasks in the background.    */
    private var backgroundHandler: Handler? = null

    /** An [ImageReader] that handles preview frame capture.   */
    private var imageReader: ImageReader? = null

    /** [CaptureRequest.Builder] for the camera preview   */
    private var previewRequestBuilder: CaptureRequest.Builder? = null

    /** [CaptureRequest] generated by [.previewRequestBuilder   */
    private var previewRequest: CaptureRequest? = null

    /** A [Semaphore] to prevent the app from exiting before closing the camera.    */
    private val cameraOpenCloseLock = Semaphore(1)

    /** Whether the current camera device supports Flash or not.    */
    private var flashSupported = false

    /** Orientation of the camera sensor.   */
    private var sensorOrientation: Int? = null

    /** Abstract interface to someone holding a display surface.    */
    private var surfaceHolder: SurfaceHolder? = null

    /** [CameraDevice.StateCallback] is called when [CameraDevice] changes its state.   */
    private val stateCallback = object : CameraDevice.StateCallback() {

        override fun onOpened(cameraDevice: CameraDevice) {
            cameraOpenCloseLock.release()
            this@PosenetActivity.cameraDevice = cameraDevice
            createCameraPreviewSession()
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            cameraOpenCloseLock.release()
            cameraDevice.close()
            this@PosenetActivity.cameraDevice = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            onDisconnected(cameraDevice)
            this@PosenetActivity.activity?.finish()
        }
    }

    /**
     * A [CameraCaptureSession.CaptureCallback] that handles events related to JPEG capture.
     */
    private val captureCallback = object : CameraCaptureSession.CaptureCallback() {
        override fun onCaptureProgressed(
                session: CameraCaptureSession,
                request: CaptureRequest,
                partialResult: CaptureResult
        ) {
        }

        override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
        ) {
        }
    }

    /**
     * Shows a [Toast] on the UI thread.
     *
     * @param text The message to show
     */
    private fun showToast(text: String) {
        val activity = activity
        activity?.runOnUiThread { Toast.makeText(activity, text, Toast.LENGTH_SHORT).show() }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.activity_posenet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        surfaceView = view.findViewById(R.id.surfaceView)
        imgWatermark = view.findViewById(R.id.imgWatermark)
        surfaceHolder = surfaceView!!.holder

        imgWatermark = view.findViewById(R.id.imgWatermark)

    }

    private var startTime: Long = 0
    private var endTime: Long = 0
    private var totalTime: Long = 0
    private val db = FirebaseFirestore.getInstance()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel = ViewModelProviders.of(activity!!).get(StudentBaseViewModel::class.java)
        missionTarget = mViewModel!!.student.missionTarget
        if (mViewModel!!.student.teamid >= 0) {
            stuLetter1.setImageResource(imagedata[mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder[0]])
            stuLetter2.setImageResource(imagedata[mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder[1]])
            stuLetter3.setImageResource(imagedata[mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder[2]])
            if (mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder.size == 4) {
                stuLetter4.setImageResource(imagedata[mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].targetOrder[3]])
            } else {
                stuLetter4.visibility = View.GONE
                stugroup4.visibility = View.GONE
            }
            currentPlayer = mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].currentOrder
            if (currentPlayer == mViewModel!!.student.teamOrder) {
                imgWatermark!!.setImageResource(imagedata[missionTarget])
                imgWatermark!!.alpha = 0.4F
                startTime = System.currentTimeMillis()
            } else {
                imgWatermark!!.alpha = 1.0F
                if (!missionDone) {
                    imgWatermark!!.setImageResource(R.drawable.banner)
                }
            }

        } else {
            noTeam = true
            imgWatermark!!.setImageResource(imagedata[missionTarget])
            imgWatermark!!.alpha = 0.4F
            startTime = System.currentTimeMillis()
        }

        db.collection("Rooms").document(mViewModel!!.room.uid).addSnapshotListener { snapshot, firebaseFirestoreException ->
            if (firebaseFirestoreException != null) {
                Log.w(TAG, "Listen failed.", firebaseFirestoreException)
                return@addSnapshotListener
            }
            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: ${snapshot.data}")
                mViewModel!!.room = snapshot.toObject(Room::class.java)
                if (mViewModel!!.student.teamid >= 0) {
                    currentPlayer = mViewModel!!.room.teams[mViewModel!!.student.teamid - 1].currentOrder
                    if (currentPlayer == mViewModel!!.student.teamOrder) {
                        imgWatermark!!.setImageResource(imagedata[missionTarget])
                        imgWatermark!!.alpha = 0.4F
                        startTime = System.currentTimeMillis()
                    } else {
                        imgWatermark!!.alpha = 1.0F
                        if (!missionDone) {
                            imgWatermark!!.setImageResource(R.drawable.banner)
                        }
                    }
                } else {
                    noTeam = true
                }
            } else {
                Log.d(TAG, "Current data: null")
            }
        }


    }


    override fun onResume() {
        super.onResume()
        startBackgroundThread()
    }

    override fun onStart() {
        super.onStart()
        openCamera()
        posenet = Posenet(this.context!!)
    }

    override fun onPause() {
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        posenet.close()
    }


    /**
     * Sets up member variables related to camera.
     */
    private fun setUpCameraOutputs() {

        val activity = activity
        val manager = activity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            for (cameraId in manager.cameraIdList) {
                val characteristics = manager.getCameraCharacteristics(cameraId)

                // We don't use a front facing camera in this sample.
                val cameraDirection = characteristics.get(CameraCharacteristics.LENS_FACING)
                if (cameraDirection != null &&
                        cameraDirection == CameraCharacteristics.LENS_FACING_FRONT
                ) {
                    continue
                }

                previewSize = Size(PREVIEW_WIDTH, PREVIEW_HEIGHT)

                imageReader = ImageReader.newInstance(
                        PREVIEW_WIDTH, PREVIEW_HEIGHT,
                        ImageFormat.YUV_420_888, /*maxImages*/ 2
                )

                sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION)!!

                previewHeight = previewSize!!.height
                previewWidth = previewSize!!.width

                // Initialize the storage bitmaps once when the resolution is known.
                rgbBytes = IntArray(previewWidth * previewHeight)

                // Check if the flash is supported.
                flashSupported =
                        characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE) == true

                this.cameraId = cameraId

                // We've found a viable camera and finished setting up member variables,
                // so we don't need to iterate through other available cameras.
                return
            }
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        } catch (e: NullPointerException) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance("Error")
                    .show(childFragmentManager, FRAGMENT_DIALOG)
        }
    }

    /**
     * Opens the camera specified by [PosenetActivity.cameraId].
     */
    private fun openCamera() {
        ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA)

        setUpCameraOutputs()
        val manager = activity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            // Wait for camera to open - 2.5 seconds is sufficient
            if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw RuntimeException("Time out waiting to lock camera opening.")
            }
            manager.openCamera(cameraId!!, stateCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera opening.", e)
        }
    }

    /**
     * Closes the current [CameraDevice].
     */
    private fun closeCamera() {
        if (captureSession == null) {
            return
        }

        try {
            cameraOpenCloseLock.acquire()
            captureSession!!.close()
            captureSession = null
            cameraDevice!!.close()
            cameraDevice = null
            imageReader!!.close()
            imageReader = null
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            cameraOpenCloseLock.release()
        }
    }

    /**
     * Starts a background thread and its [Handler].
     */
    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("imageAvailableListener").also { it.start() }
        backgroundHandler = Handler(backgroundThread!!.looper)
    }

    /**
     * Stops the background thread and its [Handler].
     */
    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e(TAG, e.toString())
        }
    }

    /** Fill the yuvBytes with data from image planes.   */
    private fun fillBytes(planes: Array<Image.Plane>, yuvBytes: Array<ByteArray?>) {
        // Row stride is the total number of bytes occupied in memory by a row of an image.
        // Because of the variable row stride it's not possible to know in
        // advance the actual necessary dimensions of the yuv planes.
        for (i in planes.indices) {
            val buffer = planes[i].buffer
            if (yuvBytes[i] == null) {
                yuvBytes[i] = ByteArray(buffer.capacity())
            }
            buffer.get(yuvBytes[i]!!)
        }
    }

    /** A [OnImageAvailableListener] to receive frames as they are available.  */
    private var imageAvailableListener = object : OnImageAvailableListener {
        override fun onImageAvailable(imageReader: ImageReader) {
            // We need wait until we have some size from onPreviewSizeChosen
            if (previewWidth == 0 || previewHeight == 0) {
                return
            }

            val image = imageReader.acquireLatestImage() ?: return
            fillBytes(image.planes, yuvBytes)

            ImageUtils.convertYUV420ToARGB8888(
                    yuvBytes[0]!!,
                    yuvBytes[1]!!,
                    yuvBytes[2]!!,
                    previewWidth,
                    previewHeight,
                    /*yRowStride=*/ image.planes[0].rowStride,
                    /*uvRowStride=*/ image.planes[1].rowStride,
                    /*uvPixelStride=*/ image.planes[1].pixelStride,
                    rgbBytes
            )

            // Create bitmap from int array
            val imageBitmap = Bitmap.createBitmap(
                    rgbBytes, previewWidth, previewHeight,
                    Bitmap.Config.ARGB_8888
            )

            // Create rotated version for portrait display
            val rotateMatrix = Matrix()
            rotateMatrix.postRotate(90.0f)

            val rotatedBitmap = Bitmap.createBitmap(
                    imageBitmap, 0, 0, previewWidth, previewHeight,
                    rotateMatrix, true
            )
            image.close()

            // Process an image for analysis in every 3 frames.
            frameCounter = (frameCounter + 1) % 3
            if (frameCounter == 0) {
                processImage(rotatedBitmap)
            }
        }
    }

    /** Crop Bitmap to maintain aspect ratio of model input.   */
    private fun cropBitmap(bitmap: Bitmap): Bitmap {
        // Rotated bitmap has previewWidth as its height and previewHeight as width.
        val previewRatio = previewWidth.toFloat() / previewHeight
        val modelInputRatio = MODEL_HEIGHT.toFloat() / MODEL_WIDTH
        var croppedBitmap = bitmap

        // Acceptable difference between the modelInputRatio and previewRatio to skip cropping.
        val maxDifference = 1.0f.pow(-5)

        // Checks if the previewing bitmap has similar aspect ratio as the required model input.
        when {
            abs(modelInputRatio - previewRatio) < maxDifference -> return croppedBitmap
            modelInputRatio > previewRatio -> {
                // New image is taller so we are height constrained.
                val cropHeight = previewHeight - (previewWidth.toFloat() / modelInputRatio)
                croppedBitmap = Bitmap.createBitmap(
                        bitmap,
                        0,
                        (cropHeight / 2).toInt(),
                        previewHeight,
                        (previewWidth - (cropHeight / 2)).toInt()
                )
            }
            else -> {
                val cropWidth = previewWidth - (previewHeight.toFloat() * modelInputRatio)
                croppedBitmap = Bitmap.createBitmap(
                        bitmap,
                        (cropWidth / 2).toInt(),
                        0,
                        (previewHeight - (cropWidth / 2)).toInt(),
                        previewWidth
                )
            }
        }
        return croppedBitmap
    }

    /** Set the paint color and size.    */
    private fun setPaint() {
        paint.color = Color.RED
        paint.textSize = 80.0f
        paint.strokeWidth = 8.0f
    }


    /** Draw bitmap on Canvas.   */
    private fun draw(canvas: Canvas, person: Person, bitmap: Bitmap) {
        val screenWidth: Int = canvas.width
        val screenHeight: Int = canvas.height
        setPaint()
        canvas.drawBitmap(
                bitmap,
                Rect(0, 0, previewHeight, previewWidth),
                Rect(0, 0, screenWidth, screenHeight),
                paint
        )

        val widthRatio = screenWidth.toFloat() / MODEL_WIDTH
        val heightRatio = screenHeight.toFloat() / MODEL_HEIGHT

        // Draw key points over the image.
        for (keyPoint in person.keyPoints) {
            if (keyPoint.score > minConfidence) {
                val position = keyPoint.position
                val adjustedX: Float = position.x.toFloat() * widthRatio
                val adjustedY: Float = position.y.toFloat() * heightRatio
                canvas.drawCircle(adjustedX, adjustedY, circleRadius, paint)
            }
        }
        surfaceHolder!!.unlockCanvasAndPost(canvas)
    }

    /** Process image using Posenet library.   */
    private fun processImage(bitmap: Bitmap) {
        // Crop bitmap.
        val croppedBitmap = cropBitmap(bitmap)

        // Created scaled version of bitmap for model input.
        val scaledBitmap = Bitmap.createScaledBitmap(croppedBitmap, MODEL_WIDTH, MODEL_HEIGHT, true)

        // Perform inference.
        val person = posenet.estimateSinglePose(scaledBitmap)
        val canvas: Canvas = surfaceHolder!!.lockCanvas()

        if (currentPlayer == mViewModel!!.student.teamOrder || noTeam) {
            checkImage(person)
        }

        draw(canvas, person, bitmap)
    }


    /**
     * Creates a new [CameraCaptureSession] for camera preview.
     */
    private fun createCameraPreviewSession() {
        try {

            // We capture images from preview in YUV format.
            imageReader = ImageReader.newInstance(
                    previewSize!!.width, previewSize!!.height, ImageFormat.YUV_420_888, 2
            )
            imageReader!!.setOnImageAvailableListener(imageAvailableListener, backgroundHandler)

            // This is the surface we need to record images for processing.
            val recordingSurface = imageReader!!.surface

            // We set up a CaptureRequest.Builder with the output Surface.
            previewRequestBuilder = cameraDevice!!.createCaptureRequest(
                    CameraDevice.TEMPLATE_PREVIEW
            )
            previewRequestBuilder!!.addTarget(recordingSurface)

            // Here, we create a CameraCaptureSession for camera preview.
            cameraDevice!!.createCaptureSession(
                    listOf(recordingSurface),
                    object : CameraCaptureSession.StateCallback() {
                        override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                            // The camera is already closed
                            if (cameraDevice == null) return

                            // When the session is ready, we start displaying the preview.
                            captureSession = cameraCaptureSession
                            try {
                                // Auto focus should be continuous for camera preview.
                                previewRequestBuilder!!.set(
                                        CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
                                )
                                // Flash is automatically enabled when necessary.
                                setAutoFlash(previewRequestBuilder!!)

                                // Finally, we start displaying the camera preview.
                                previewRequest = previewRequestBuilder!!.build()
                                captureSession!!.setRepeatingRequest(
                                        previewRequest!!,
                                        captureCallback, backgroundHandler
                                )
                            } catch (e: CameraAccessException) {
                                Log.e(TAG, e.toString())
                            }
                        }

                        override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
                            showToast("Failed")
                        }
                    },
                    null
            )
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        }
    }

    private fun setAutoFlash(requestBuilder: CaptureRequest.Builder) {
        if (flashSupported) {
            requestBuilder.set(
                    CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH
            )
        }
    }

    /**
     * Shows an error message dialog.
     */
    class ErrorDialog : DialogFragment() {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
                AlertDialog.Builder(activity)
                        .setMessage(arguments!!.getString(ARG_MESSAGE))
                        .setPositiveButton(android.R.string.ok) { _, _ -> activity!!.finish() }
                        .create()

        companion object {

            @JvmStatic
            private val ARG_MESSAGE = "message"

            @JvmStatic
            fun newInstance(message: String): ErrorDialog = ErrorDialog().apply {
                arguments = Bundle().apply { putString(ARG_MESSAGE, message) }
            }
        }
    }

    companion object {
        /**
         * Conversion from screen rotation to JPEG orientation.
         */
        private val ORIENTATIONS = SparseIntArray()
        private val FRAGMENT_DIALOG = "dialog"

        init {
            ORIENTATIONS.append(Surface.ROTATION_0, 90)
            ORIENTATIONS.append(Surface.ROTATION_90, 0)
            ORIENTATIONS.append(Surface.ROTATION_180, 270)
            ORIENTATIONS.append(Surface.ROTATION_270, 180)
        }

        /**
         * Tag for the [Log].
         */
        private const val TAG = "PosenetActivity"
    }
}
