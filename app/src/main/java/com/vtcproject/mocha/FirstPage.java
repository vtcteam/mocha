package com.vtcproject.mocha;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.vtcproject.mocha.student.StudentBase;
import com.vtcproject.mocha.teacher.TeacherBase;

public class FirstPage extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);
        findViewById(R.id.firstPageStudentBtn).setOnClickListener(this);
        findViewById(R.id.firstPageTeacherBtn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.firstPageStudentBtn:
                intent = new Intent(this, StudentBase.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.firstPageTeacherBtn:
                intent = new Intent(this, TeacherBase.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
        }
    }
}
