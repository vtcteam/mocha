package com.vtcproject.mocha.models;

public class Student {

    private String uid;
    private String name;
    private String classid; //看需不需要留這個
    private int teamid = -1; //default -1
    private int teamOrder;
    private int status; // 0 = Not Ready, 1 = Ready, 2 = Playing, 3 = Finished
    private long startTime;
    private long endTime;
    private long totalTime;
    private int missionTarget;


    public Student() {


    }

    public int getTeamOrder() {
        return teamOrder;
    }

    public void setTeamOrder(int teamOrder) {
        this.teamOrder = teamOrder;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTeamid() {
        return teamid;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public int getMissionTarget() {
        return missionTarget;
    }

    public void setMissionTarget(int missionTarget) {
        this.missionTarget = missionTarget;
    }

    public void goNextRound(){
        this.startTime = 0;
        this.endTime = 0;
        this.missionTarget = 0;
        this.teamid = -1;
        this.teamOrder = 0;
        this.status = 0;
        totalTime = 0;

    }
    @Override
    public String toString() {
        return "Student{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", classid='" + classid + '\'' +
                ", status=" + status +
                '}';
    }

    public String getStatusString(){
        switch (status){
            case 0:
                return "Not ready";
            case 1:
                return "Ready";
            case 2:
                return "Playing";
            case 3:
                return "Finished";
                default: return null;
        }
    }

}
