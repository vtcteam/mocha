package com.vtcproject.mocha.models;


import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class Room {
    private String uid;
    private String roomId;
    private String fixRoomId;
    private String teacherId;
    private String gameId;
    private int roomStatus;  //0 Wait, 1 Start, 2 End
    private int round;
    private String nextRoundRoomUid;
    private ArrayList<Student> students;
    private ArrayList<Team> teams;
    private Long startTime;

    public Room() {
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public String getFixRoomId() {
        return fixRoomId;
    }

    public void setFixRoomId(String fixRoomId) {
        this.fixRoomId = fixRoomId;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getNextRoundRoomUid() {
        return nextRoundRoomUid;
    }

    public void setNextRoundRoomUid(String nextRoundRoomUid) {
        this.nextRoundRoomUid = nextRoundRoomUid;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public int getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(int roomStatus) {
        this.roomStatus = roomStatus;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId='" + roomId + '\'' +
                ", roomStatus=" + roomStatus +
                ", students=" + students +
                '}';
    }

    public void updateStudent(Student student) {
        if (students != null) {
            if (students.size() <= findIndexByUid(student.getUid())) {
                students.add(student);
            } else {
                students.set(findIndexByUid(student.getUid()), student);
            }
        } else {
            students = new ArrayList<>();
            students.add(student);
        }
    }

    private int findIndexByUid(String uid) {
        int idx = 0;
        if (students != null) {
            for (Student student : students) {
                if (student.getUid().equals(uid)) {
                    break;
                }
                idx++;
            }
        }
        return idx;
    }

    public Student getStudent(String uid){
        return students.get(findIndexByUid(uid));
    }

    public void kickStudent(String uid) {
        students.remove(findIndexByUid(uid));
    }

    public boolean checkReady() {
        boolean ready = true;
        if (students != null) {
            for (Student student : students) {
                if (student.getStatus() != 1) {
                    System.out.println(student.getStatus());
                    ready = false;
                }
            }
        }else{
            ready = false;
        }

        Log.d("Room Class", "" + ready);
        return ready;
    }

    public void startGame() {
        for (Student student : students) {
            student.setStatus(2);
            student.setMissionTarget(new Random().nextInt(9));
        }
        if (teams != null){
            for (Student student : students){
                teams.get(student.getTeamid()-1).addTargetOrder(student.getMissionTarget());
            }
        }
        setStartTime(System.currentTimeMillis());
        setRoomId("");
        setRoomStatus(1);
    }

    public int checkTeam(String uid){
        int idx = findIndexByUid(uid);
        Student student = students.get(idx);
        return student.getTeamid();
    }

    public void updateTeamOrder(int teamId, int newOrder){
        teams.get(teamId-1).setCurrentOrder(newOrder);
    }

    public void endGame() {
        setRoomStatus(2);
        for (Student student : students){
            if (student.getStatus() == 2){
                student.setEndTime(System.currentTimeMillis());
                student.setTotalTime(System.currentTimeMillis() - startTime);
            }
        }
    }
}
