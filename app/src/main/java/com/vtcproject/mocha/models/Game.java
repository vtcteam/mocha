package com.vtcproject.mocha.models;

import java.util.ArrayList;

public class Game {
    private String uid;
    private ArrayList<Room> roomList;

    public Game() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(ArrayList<Room> roomList) {
        this.roomList = roomList;
    }

    public void addRoom(Room room){
        if (roomList == null){
            roomList = new ArrayList<>();
        }
        roomList.add(room);
    }

    public void updateRoom(Room room){
        roomList.set(findIndexByUid(room.getUid()), room);
    }

    private int findIndexByUid(String uid) {
        int idx = 0;
        if (roomList != null) {
            for (Room room : roomList) {
                if (room.getUid().equals(uid)) {
                    break;
                }
                idx++;
            }
        }
        return idx;
    }
}


