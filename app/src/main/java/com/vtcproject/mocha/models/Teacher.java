package com.vtcproject.mocha.models;

import java.util.ArrayList;

public class Teacher {
    private String uid;
    private ArrayList<Game> gameHistory;


    public Teacher() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<Game> getGameHistory() {
        return gameHistory;
    }

    public void setGameHistory(ArrayList<Game> gameHistory) {
        this.gameHistory = gameHistory;
    }

    public void newGame(String gameid) {
        if (gameHistory == null){
            gameHistory = new ArrayList<>();
        }
        Game tmpGame = new Game();
        tmpGame.setUid(gameid);
        gameHistory.add(tmpGame);
    }

    public Game getGame(String gameid){
        return gameHistory.get(findIndexByUid(gameid));
    }

    public void addRound(Room room, String gameid){
        gameHistory.get(findIndexByUid(gameid)).addRoom(room);
    }
    public void updateRound(Room room, String gameid){
        gameHistory.get(findIndexByUid(gameid)).updateRoom(room);
    }

    private int findIndexByUid(String uid) {
        int idx = 0;
        if (gameHistory != null) {
            for (Game game : gameHistory) {
                if (game.getUid().equals(uid)) {
                    break;
                }
                idx++;
            }
        }
        return idx;
    }
}
