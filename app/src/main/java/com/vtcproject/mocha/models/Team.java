package com.vtcproject.mocha.models;

        import java.util.ArrayList;

public class Team {
    private String uid;
    private boolean allDone = false;
    private int currentOrder = 0;
    private ArrayList<Student> students;
    private ArrayList<Integer> targetOrder;
    private Long totalTime;



    public Team() {
        students = new ArrayList<>();
        targetOrder = new ArrayList<>();
        totalTime = 0L;
    }

    public Long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public boolean isAllDone() {
        return allDone;
    }

    public ArrayList<Integer> getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(ArrayList<Integer> targetOrder) {
        this.targetOrder = targetOrder;
    }

    public void addTargetOrder(int Order){
        targetOrder.add(Order);
    }



    public void setAllDone(boolean allDone) {
        this.allDone = allDone;
    }

    public int getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(int currentOrder) {
        this.currentOrder = currentOrder;
    }
}
