package com.vtcproject.mocha.student.ui.student;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.posenet.PosenetActivity;
import com.vtcproject.mocha.student.StudentBaseViewModel;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.TimeZone;


public class ReadyRoom extends BaseFragment {
    private FirebaseFirestore db;
    private StudentBaseViewModel mViewModel;
    private Button readyCB;
    private boolean firstTime = true; //used for renew cache db first
    private boolean permissionCheck = false;
    private boolean isChecked = false;
    private TextView teamIdTV;
    private TextView roomId;

    public ReadyRoom() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_ready_room, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(activity).get(StudentBaseViewModel.class);
        checkAndRequestCameraPermission();
        teamIdTV = activity.findViewById(R.id.stuTeamID);
        roomId = activity.findViewById(R.id.RoomNumberTV);
        roomId.setText("Room Number " + mViewModel.room.getFixRoomId());

        db = FirebaseFirestore.getInstance();
        RoomListener = db.collection("Rooms").document(mViewModel.room.getUid()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null && !firstTime) {
                    mViewModel.room = documentSnapshot.toObject(Room.class);
                    if (mViewModel.room != null) {
                        mViewModel.student = mViewModel.room.getStudent(mViewModel.student.getUid());
                        System.out.println(mViewModel.room.toString());
                        if (mViewModel.room.checkTeam(mViewModel.student.getUid()) > -1){
                            teamIdTV.setVisibility(View.VISIBLE);
                            teamIdTV.setText(getString(R.string.ReadyRoomTeamId, mViewModel.room.checkTeam(mViewModel.student.getUid())));
                        }
                        if (mViewModel.room.getRoomStatus() == 1 && permissionCheck) {
                            activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new PosenetActivity()).commitNow();
                        }
                    }
                }
                firstTime = false;
            }
        });
        readyCB = activity.findViewById(R.id.readyRoomCB);
        readyCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChecked = !isChecked;
                if (!permissionCheck){
                    checkAndRequestCameraPermission();
                    isChecked = false;
                }

                if (isChecked && permissionCheck) {
                    readyCB.setClickable(false);
                    mViewModel.student.setStatus(1);
                    mViewModel.room.updateStudent(mViewModel.student);
                    db.runTransaction(new Transaction.Function<Room>() {
                        @Nullable
                        @Override
                        public Room apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                            DocumentReference documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
                            DocumentSnapshot documentSnapshot = transaction.get(documentReference);
                            transaction.update(documentReference, "students", mViewModel.room.getStudents());
                            return documentSnapshot.toObject(Room.class);
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Room>() {
                        @Override
                        public void onSuccess(Room room) {
                            mViewModel.room = room;
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("ReadyRoom", "Fail: " + e + " " + e.getMessage());
                        }
                    });
                }
            }
        });

    }

    private void checkAndRequestCameraPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        }else{
            permissionCheck = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        System.out.println(grantResults[0] + "...." + permissions[0]);
        if (permissions[0].equals(Manifest.permission.CAMERA) && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            permissionCheck = true;
        }else{
            Toast.makeText(activity, "You must allow carera permission to ready", Toast.LENGTH_LONG).show();
        }
    }
}
