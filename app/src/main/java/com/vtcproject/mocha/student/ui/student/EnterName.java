package com.vtcproject.mocha.student.ui.student;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.student.StudentBaseViewModel;
import com.vtcproject.mocha.R;


public class EnterName extends BaseFragment {

    private StudentBaseViewModel mViewModel;
    private TextInputEditText stuNameET, stuClassIdET;
    private TextInputLayout stuNameL, stuClassIdL;

    public static EnterName newInstance() {
        return new EnterName();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student_enter_name, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(activity).get(StudentBaseViewModel.class);
        stuNameET = activity.findViewById(R.id.stuEnterNameETname);
        stuClassIdET = activity.findViewById(R.id.stuEnterNameETClassID);
        stuNameL = activity.findViewById(R.id.stuNameInputLayout);
        stuClassIdL = activity.findViewById(R.id.stuClassIdInputLayout);

        activity.findViewById(R.id.stuEnterNamebtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stuNameET.getText() != null && !stuNameET.getText().toString().equals("")){
                    String stuName = stuNameET.getText().toString();
                    mViewModel.student.setName(stuName);
                    stuNameL.setError("");
                }else{
                    stuNameL.setError("Name cannot empty");
                    return;
                }

                if (stuClassIdET.getText() != null && !stuClassIdET.getText().toString().equals("")) {
                    String stuClassId = stuClassIdET.getText().toString();
                    mViewModel.student.setClassid(stuClassId);
                    stuClassIdL.setError("");
                }else{
                    stuClassIdL.setError("Class ID cannot empty");
                    return;
                }
                UpdateStudent(mViewModel.student.getUid(), mViewModel.student);
                //activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new PosenetActivity()).commitNow();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new EnterRoom()).commitNow();
            }
        });
    }
}
