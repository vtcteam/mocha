package com.vtcproject.mocha.student;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;


public class StudentBaseViewModel extends ViewModel {

    public Student student = new Student();
    public Room room = new Room();

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d("onClear", "Clear");
    }
}
