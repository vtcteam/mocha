package com.vtcproject.mocha.student.ui.student;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.student.StudentBaseViewModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class EnterRoom extends BaseFragment {

    private StudentBaseViewModel mViewModel;
    private TextInputEditText roomNumET;
    private TextInputLayout roomNumIL;
    private Button stuEnterRoomBtn;
    private FirebaseFirestore db;
    private String roomNum;
    DocumentReference documentReference;


    public EnterRoom() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_enter_room, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        roomNumET = activity.findViewById(R.id.stuRoomNumInput);
        roomNumIL = activity.findViewById(R.id.stuRoomNumLayout);
        mViewModel = ViewModelProviders.of(activity).get(StudentBaseViewModel.class);
        System.out.println("0"+ mViewModel.room);
        stuEnterRoomBtn = activity.findViewById(R.id.stuEnterRoombtn);
        stuEnterRoomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                if (roomNumET.getText() != null && !roomNumET.getText().toString().equals("")){
                    roomNum = roomNumET.getText().toString();
                    roomNumIL.setError("");
                }else{
                    roomNumIL.setError("Room number cannot empty");
                    hideProgressDialog();
                    return;
                }
                db.collection("Rooms").whereEqualTo("roomId", roomNum).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        String roomUid;
                        Log.e("EnterRoom", "0");
                        if (task.getResult() != null && task.getResult().getDocuments().size() > 0){
                            Log.e("EnterRoom", "Something");
                            roomUid = task.getResult().getDocuments().get(0).getId();
                            documentReference = db.collection("Rooms").document(roomUid);
                            db.runTransaction(new Transaction.Function<Room>() {
                                @Nullable
                                @Override
                                public Room apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                                    DocumentSnapshot documentSnapshot = transaction.get(documentReference);
                                    mViewModel.room = documentSnapshot.toObject(Room.class);
                                    if (mViewModel.room == null){
                                        throw new FirebaseFirestoreException("No This Room", FirebaseFirestoreException.Code.NOT_FOUND);
                                    }
                                    mViewModel.room.updateStudent(mViewModel.student);
                                    transaction.update(documentReference, "students", mViewModel.room.getStudents());
                                    return mViewModel.room;
                                }
                            }).addOnSuccessListener(new OnSuccessListener<Room>() {
                                @Override
                                public void onSuccess(Room room) {
                                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new ReadyRoom()).commitNow();
                                    hideProgressDialog();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    roomNumIL.setError("Room number is not exist, please ask your teacher for room number.");
                                    Log.e("EnterRoom","Fail: " + e + " " + e.getMessage());
                                    hideProgressDialog();
                                }
                            });
                        }else{
                            roomNumIL.setError("Room number is not exist, please ask your teacher for room number.");
                            hideProgressDialog();
                        }
                    }
                });
            }
        });
    }
}
