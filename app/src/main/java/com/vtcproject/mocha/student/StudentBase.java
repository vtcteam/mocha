package com.vtcproject.mocha.student;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vtcproject.mocha.BaseActivity;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.student.ui.student.EnterName;

import java.util.Objects;

public class StudentBase extends BaseActivity {
    public static String TAG = "Student";
    private StudentBaseViewModel mViewModel;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_base_activity);

        mViewModel = ViewModelProviders.of(this).get(StudentBaseViewModel.class);
        showProgressDialog();
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    mViewModel.student.setUid(mAuth.getUid());
                    UpdateStudent();
                    hideProgressDialog();
                }else{
                    Log.e(TAG, "Sign in Fail",task.getException());
                }
            }
        });


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, EnterName.newInstance())
                    .commitNow();
        }
    }

    public void UpdateStudent(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Students").document(mViewModel.student.getUid()).set(mViewModel.student);
        Log.i(TAG, "Do update");
    }
}
