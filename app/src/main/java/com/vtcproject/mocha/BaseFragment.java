package com.vtcproject.mocha;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.vtcproject.mocha.models.Student;

public abstract class BaseFragment extends Fragment {

    public FragmentActivity activity;
    private ProgressDialog mProgressDialog;
    public ListenerRegistration RoomListener;

    public void UpdateStudent(String uid, Student student){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Students").document(uid).set(student);
        Log.i("EnterName", "Do Student update");
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(activity);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (RoomListener != null) RoomListener.remove();
    }
}
