package com.vtcproject.mocha.teacher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.vtcproject.mocha.BaseActivity;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.teacher.ui.teacher.LoginPage;

public class TeacherBase extends BaseActivity {
    private TeacherBaseViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher_base_activity);
        viewModel = ViewModelProviders.of(this).get(TeacherBaseViewModel.class);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, LoginPage.newInstance())
                    .commitNow();
        }
    }


}
