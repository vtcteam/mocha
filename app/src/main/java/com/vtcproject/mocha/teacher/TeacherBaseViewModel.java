package com.vtcproject.mocha.teacher;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Teacher;

public class TeacherBaseViewModel extends ViewModel {
    public Room room = new Room();
    public Teacher teacher = new Teacher();
    public String gameId;
    public String summaryId;

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d("onClear", "Clear");
    }
}
