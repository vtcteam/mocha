package com.vtcproject.mocha.teacher.ui.teacher;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Transaction;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GameRoom extends BaseFragment {
    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private DocumentReference documentReference;
    private GameStatusListAdapter gameStatusListAdapter;
    private RecyclerView recyclerView;


    public GameRoom() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teacher_game_room, container, false);

    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);

        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());

        recyclerView = activity.findViewById(R.id.rvStudentStatus);

        final Button button = activity.findViewById(R.id.BtnEndGame);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.room.endGame();
                mViewModel.teacher.addRound(mViewModel.room, mViewModel.room.getGameId());
                db.collection("Teachers").document(mViewModel.teacher.getUid()).set(mViewModel.teacher);
                if (mViewModel.room.getRound() < 3){
                    mViewModel.room.setUid(mViewModel.room.getNextRoundRoomUid());
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new ReadyRoom()).commitNow();
                }else{
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new Summary()).commitNow();
                }
            }
        });

        RoomListener = documentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null) {
                    mViewModel.room = documentSnapshot.toObject(Room.class);
                    if (mViewModel.room != null && mViewModel.room.getStudents() != null) {
                        gameStatusListAdapter = new GameStatusListAdapter(activity, activity.getApplicationContext(), mViewModel.room.getStudents(), mViewModel.room);
                        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                        recyclerView.setAdapter(gameStatusListAdapter);
                    }
                }
            }
        });

    }

}

class GameStatusListAdapter extends RecyclerView.Adapter<GameStatusListAdapter.GameStatusViewHolder> {
    private FragmentActivity activity;
    private Context context;
    private ArrayList<Student> mData;
    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private DocumentReference documentReference;
    private Room room;

    GameStatusListAdapter(FragmentActivity activity, Context context, ArrayList<Student> mData, Room room) {
        this.activity = activity;
        this.context = context;
        this.mData = mData;
        this.room = room;
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        db = FirebaseFirestore.getInstance();
        System.out.println(mViewModel.room.getUid());
        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
    }

    @NonNull
    @Override
    public GameStatusViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_teacher_game_status, parent, false);
        GameStatusViewHolder holder = new GameStatusViewHolder(view);
        holder.tvTeamID = view.findViewById(R.id.tvTeamID);
        holder.tvStatus = view.findViewById(R.id.tvStatus);
        holder.tvTime = view.findViewById(R.id.tvCompletedTime);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GameStatusListAdapter.GameStatusViewHolder holder, int position) {
        final Student student = mData.get(position);
        if (room.getTeams() != null) {
            holder.tvTeamID.setText(Integer.toString(position+1));
            if (room.getTeams().get(position).isAllDone()){
                holder.tvStatus.setText("Finished");
                holder.tvTime.setText(Double.toString(room.getTeams().get(position).getTotalTime() / 1000.0));
            }

        } else {
            holder.tvTeamID.setText(student.getName());
            holder.tvStatus.setText(student.getStatusString());
            holder.tvTime.setText(Double.toString(student.getTotalTime() / 1000.0));
        }



    }

    @Override
    public int getItemCount() {
        if (room.getTeams() != null) {
            return room.getTeams().size();
        } else {
            return mData.size();
        }
    }

    class GameStatusViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamID, tvStatus, tvTime;
        GameStatusViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}