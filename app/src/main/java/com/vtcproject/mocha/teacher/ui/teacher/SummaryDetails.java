package com.vtcproject.mocha.teacher.ui.teacher;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Game;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.models.Teacher;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SummaryDetails extends BaseFragment {
    private FirebaseFirestore db;
    private TeacherBaseViewModel mViewModel;
    private TextView tvSumDetailsStudentName;
    private RecyclerView rvSumDetailStuResult;
    private SummaryDetailsListAdapter summaryDetailsListAdapter;
    private ArrayList<Room> roomList;


    public SummaryDetails(){
   }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_summary_details, container, false);
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvSumDetailsStudentName = activity.findViewById(R.id.tvSumDetailsStudentName);

        db = FirebaseFirestore.getInstance();
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        roomList = new ArrayList<>(mViewModel.teacher.getGame(mViewModel.gameId).getRoomList());
        ArrayList<Double> time = new ArrayList<>();
        String studentName = "";

        for (Room room: roomList){
            Student student = room.getStudent(mViewModel.summaryId);
            studentName = student.getName();
            time.add(student.getTotalTime() / 1000.0);
        }


        tvSumDetailsStudentName.setText(studentName);

        rvSumDetailStuResult = activity.findViewById(R.id.rvSumDetailStuResult);
        summaryDetailsListAdapter = new SummaryDetailsListAdapter(activity, activity.getApplicationContext(), time);
        rvSumDetailStuResult.setLayoutManager(new LinearLayoutManager(activity));
        rvSumDetailStuResult.setAdapter(summaryDetailsListAdapter);
        Button btnBackList = activity.findViewById(R.id.btnBackList);
        btnBackList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new Summary()).commitNow();
            }
        });

    }




}

class SummaryDetailsListAdapter extends RecyclerView.Adapter<SummaryDetailsListAdapter.SummaryDetailsViewHolder> {
    private FragmentActivity activity;
    private Context context;
    private ArrayList<Double> mData;
    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private DocumentReference documentReference;
    private Room room;
    private Teacher teacher;

    SummaryDetailsListAdapter(FragmentActivity activity, Context context, ArrayList<Double> time) {
        this.activity = activity;
        this.context = context;
        this.mData = time;

        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);


    }

    @NonNull
    @Override
    public SummaryDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_teacher_summary_details, parent, false);
        SummaryDetailsViewHolder holder = new SummaryDetailsViewHolder(view);
        holder.tvSumDetailRound = view.findViewById(R.id.tvSumDetailRound);
        holder.tvSumDetailTime = view.findViewById(R.id.tvSumDetailTime);
        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull SummaryDetailsListAdapter.SummaryDetailsViewHolder holder, int position) {

        holder.tvSumDetailRound.setText(Integer.toString(position+1));
        holder.tvSumDetailTime.setText(Double.toString(mData.get(position)));
    }


    @Override
    public int getItemCount() {
     return mData.size();
    }

    class SummaryDetailsViewHolder extends RecyclerView.ViewHolder {
        TextView tvSumDetailRound, tvSumDetailTime;
        SummaryDetailsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}