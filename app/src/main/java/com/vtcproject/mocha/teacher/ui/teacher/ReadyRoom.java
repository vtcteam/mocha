package com.vtcproject.mocha.teacher.ui.teacher;


import android.content.Context;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.models.Team;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.util.ArrayList;

public class ReadyRoom extends BaseFragment {

    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private RecyclerView recyclerView;
    private StudentListAdapter studentListAdapter;
    private TextView stfReadyRoomNumTV;
    private Button startBtn, createTeamBtn;
    private DocumentReference documentReference;
    private ArrayList<Team> teamsForRoom;
    private ArrayList<Student> studentForTeam;
    private String nextRoomUID;
    private Room nextRoom;

    public ReadyRoom() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_ready_room, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
        recyclerView = activity.findViewById(R.id.stfStudentReadyList);
        stfReadyRoomNumTV = activity.findViewById(R.id.stfReadyRoomNum);
        startBtn = activity.findViewById(R.id.stfStartBtn);
        createTeamBtn = activity.findViewById(R.id.stfCreateTeam);

        createTeamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.room.checkReady()) {
                    if (mViewModel.room.getRound() == 1) {
                        RoundOneTeam();
                    }
                }else{
                    Toast.makeText(activity, "Please Wait all student ready.", Toast.LENGTH_LONG).show();
                }
            }
        });

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.room.checkReady()) {
                    createNextRoom();
                }else{
                    Toast.makeText(activity, "Please Wait all student ready.", Toast.LENGTH_LONG).show();
                }
            }
        });

        stfReadyRoomNumTV.setText(getString(R.string.ReadyRoomNumber, mViewModel.room.getRoomId()));

        RoomListener = documentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null) {
                    mViewModel.room = documentSnapshot.toObject(Room.class);
                    if (mViewModel.room != null && mViewModel.room.getStudents() != null) {
                        studentListAdapter = new StudentListAdapter(activity, activity.getApplicationContext(), mViewModel.room.getStudents());
                        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                        recyclerView.setAdapter(studentListAdapter);
                    }
                }
            }
        });
    }



    private void createTeam(int teamSize, int teamId) {
        Team tmpTeam = new Team();
        for (int i = 0; i < teamSize; i++) {
            Student tmpStudent = studentForTeam.get(0);
            tmpStudent.setTeamOrder(i);
            tmpStudent.setTeamid(teamId + 1);
            tmpTeam.addStudent(tmpStudent);
            studentForTeam.remove(0);
        }
        teamsForRoom.add(tmpTeam);
    }

    private void RoundOneTeam(){
        int numOfTeam;
        int numOfStudent = mViewModel.room.getStudents().size();
        int numOfTeamWithThree = 0;
        if (numOfStudent < 6) {
            //At least 6 student to form 2 teams within 3student
            Toast.makeText(activity, "Please wait at least 6 students enter room", Toast.LENGTH_SHORT).show();
            return;
        }
        teamsForRoom = new ArrayList<>();
        studentForTeam = new ArrayList<>(mViewModel.room.getStudents());
        if (numOfStudent % 4 == 0) {
            numOfTeam = numOfStudent / 4;
        } else {
            numOfTeam = numOfStudent / 4 + 1;
            numOfTeamWithThree = 4 - (numOfStudent % 4);
        }
        for (int i = 0; i < numOfTeam; i++) {
            if (i < numOfTeamWithThree) {
                createTeam(3, i);
            } else {
                createTeam(4, i);
            }
        }
        mViewModel.room.setTeams(teamsForRoom);
        documentReference.set(mViewModel.room);
    }

    private void createNextRoom() {
        nextRoom = new Room();
        nextRoom.setGameId(mViewModel.room.getGameId());
        nextRoom.setRoomId(mViewModel.room.getRoomId());
        nextRoom.setFixRoomId(mViewModel.room.getFixRoomId());
        nextRoom.setTeacherId(mViewModel.teacher.getUid());
        String id = db.collection("Rooms").document().getId();
        nextRoomUID = id;
        nextRoom.setUid(id);
        nextRoom.setRound(mViewModel.room.getRound()+1);
        db.collection("Rooms").document(id).set(nextRoom).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                db.runTransaction(new Transaction.Function<Void>() {
                    @Nullable
                    @Override
                    public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                        DocumentSnapshot documentSnapshot = transaction.get(documentReference);
                        mViewModel.room = documentSnapshot.toObject(Room.class);
                        if (mViewModel.room != null) {
                            mViewModel.room.setNextRoundRoomUid(nextRoomUID);
                            mViewModel.room.startGame();
                            transaction.set(documentReference, mViewModel.room);
                        }
                        return null;
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new GameRoom()).commitNow();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("", "Transaction failure.", e);
                    }
                });
            }
        });
    }
}


class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.ViewHolder> {
    private FragmentActivity activity;
    private Context context;
    private ArrayList<Student> mData;
    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private DocumentReference documentReference;


    StudentListAdapter(FragmentActivity activity, Context context, ArrayList<Student> mData) {
        this.activity = activity;
        this.context = context;
        this.mData = mData;
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        db = FirebaseFirestore.getInstance();
        System.out.println(mViewModel.room.getUid());
        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_teacher_ready_room, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.studentNameTV = view.findViewById(R.id.stfStudentName);
        holder.teamIdTV = view.findViewById(R.id.stfTeamId);
        holder.statusTV = view.findViewById(R.id.stfStatus);
        holder.kickBtn = view.findViewById(R.id.stfKickbtn);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentListAdapter.ViewHolder holder, int position) {
        final Student student = mData.get(position);


        if (student.getTeamid() >= 0) {
            holder.teamIdTV.setText(Integer.toString(student.getTeamid()));
        } else {
            holder.teamIdTV.setText(" ");
        }
        holder.studentNameTV.setText(student.getName());
        holder.statusTV.setText(student.getStatusString());


        holder.kickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(student.getUid());
                db.runTransaction(new Transaction.Function<Room>() {
                    @Nullable
                    @Override
                    public Room apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
                        DocumentSnapshot documentSnapshot = transaction.get(documentReference);
                        mViewModel.room = documentSnapshot.toObject(Room.class);
                        if (mViewModel.room != null) {
                            mViewModel.room.kickStudent(student.getUid());
                        }
                        transaction.update(documentReference, "students", mViewModel.room.getStudents());
                        return null;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView teamIdTV, studentNameTV, statusTV;
        ImageButton studentDetailsBtn, kickBtn;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}