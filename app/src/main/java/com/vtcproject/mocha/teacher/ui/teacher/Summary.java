package com.vtcproject.mocha.teacher.ui.teacher;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Summary extends BaseFragment {

    private ArrayList<SummaryItem> summaryList = new ArrayList<>();
    private RecyclerView recyclerView;

    public Summary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_game_summary, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TeacherBaseViewModel mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        ArrayList<Room> roomList = new ArrayList<>(mViewModel.teacher.getGame(mViewModel.gameId).getRoomList());
        for (Room room: roomList){
            for (Student student: room.getStudents()){
                SummaryItem summaryItem = new SummaryItem(student.getUid(), student.getName());
                Double totalTime = student.getTotalTime() / 1000.0;
                if (summaryList.size() == 0 || findStudentInList(student.getUid()) == -1){
                    summaryItem.setTotaltime(totalTime);
                    summaryList.add(summaryItem);
                }else{
                    totalTime += summaryList.get(findStudentInList(student.getUid())).getTotaltime();
                    summaryItem.setTotaltime(totalTime);
                    summaryList.set(findStudentInList(student.getUid()), summaryItem);
                }

            }
        }
        recyclerView = activity.findViewById(R.id.summaryRecyclerView);
        SummaryAdapter summaryAdapter = new SummaryAdapter(activity, activity.getApplicationContext(), summaryList);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(summaryAdapter);
    }

    private int findStudentInList(String uid) {
        int idx = 0;
        if (summaryList != null) {
            for (SummaryItem summaryItem : summaryList) {
                if (summaryItem.getUid().equals(uid)) {
                    break;
                }
                idx++;
            }
            if (idx >= summaryList.size()){
                idx = -1;
            }
        }
        return idx;
    }
}

class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.ViewHolder>{
    private FragmentActivity activity;
    private Context context;
    private ArrayList<SummaryItem> summaryList;
    private TeacherBaseViewModel mViewModel;

    public SummaryAdapter(FragmentActivity activity, Context context, ArrayList<SummaryItem> summaryList) {
        this.context = context;
        this.summaryList = summaryList;
        this.activity =activity;
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);

    }

    @NonNull
    @Override
    public SummaryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_teacher_game_summary, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.tvStudentName = view.findViewById(R.id.tvStudentName);
        holder.tvTotalTime = view.findViewById(R.id.tvTotalTime);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SummaryAdapter.ViewHolder holder, int position) {
        final SummaryItem summaryItem = summaryList.get(position);
        holder.tvStudentName.setText(summaryItem.getName());
        holder.tvTotalTime.setText(Double.toString(summaryItem.getTotaltime()));
        holder.tvStudentName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.summaryId = summaryItem.getUid();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new SummaryDetails()).commitNow();
            }
        });
        holder.tvTotalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.summaryId = summaryItem.getUid();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new SummaryDetails()).commitNow();
            }
        });
    }

    @Override
    public int getItemCount() {
        return summaryList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvStudentName, tvTotalTime;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}

class SummaryItem{
    private String uid;
    private String name;
    private Double totaltime;

    public SummaryItem() {
    }

    public SummaryItem(String uid, String name) {
        this.uid = uid;
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotaltime() {
        return totaltime;
    }

    public void setTotaltime(Double totaltime) {
        this.totaltime = totaltime;
    }
}