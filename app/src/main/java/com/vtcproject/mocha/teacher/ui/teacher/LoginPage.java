package com.vtcproject.mocha.teacher.ui.teacher;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Teacher;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

public class LoginPage extends BaseFragment {

    private TextInputEditText stfEmailET, stfPasswordET; //EditText
    private TextInputLayout stfEmailIL, stfPasswordIL;  // InputLayout

    private FirebaseAuth mAuth;
    private TeacherBaseViewModel mViewModel;

    private FirebaseFirestore db;

    public static LoginPage newInstance() {
        return new LoginPage();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_login_page, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        db = FirebaseFirestore.getInstance();


        stfEmailET = activity.findViewById(R.id.stfEmailET);
        stfEmailIL = activity.findViewById(R.id.stfEmailIL);
        stfPasswordET = activity.findViewById(R.id.stfPasswordET);
        stfPasswordIL = activity.findViewById(R.id.stfPasswordIL);

        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);



        mAuth = FirebaseAuth.getInstance();
        activity.findViewById(R.id.stfLoginbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new CreateRoom()).commitNow();  //Test Only Please Delete later
                String email, password;
                /*
                if (stfEmailET.getText() != null && !stfEmailET.getText().toString().equals("")){
                    email = stfEmailET.getText().toString();

                    stfEmailIL.setError("");
                }else{
                    stfEmailIL.setError("Email cannot empty");
                    return;
                }

                if (stfPasswordET.getText() != null && !stfPasswordET.getText().toString().equals("")) {
                    password = stfPasswordET.getText().toString();

                    stfPasswordIL.setError("");
                }else{
                    stfPasswordIL.setError("Password cannot empty");
                    return;
                }
                */
                email = "teacher@test.ac"; //Test only
                password = "123456"; //Test only
                showProgressDialog();
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult() != null && task.getResult().getUser() != null){
                                mViewModel.teacher.setUid(task.getResult().getUser().getUid());
                                db.collection("Teachers").document(task.getResult().getUser().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot != null){
                                            Teacher tmpTeacher = documentSnapshot.toObject(Teacher.class);
                                            if (tmpTeacher != null && tmpTeacher.getUid() != null) {
                                                mViewModel.teacher = tmpTeacher;
                                            }
                                        }
                                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new CreateRoom()).commitNow();

                                    }
                                });
                            }
                        }else {
                            try {
                                if (task.getException() != null) throw task.getException();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                switch (e.getErrorCode()) {
                                    case "ERROR_WRONG_PASSWORD":
                                        stfPasswordIL.setError("Password is invalid");
                                        stfPasswordIL.requestFocus();
                                        break;
                                    case "ERROR_INVALID_EMAIL":
                                        stfEmailIL.setError("Email is invalid");
                                        stfEmailIL.requestFocus();
                                        break;
                                }
                            } catch (FirebaseAuthInvalidUserException e) {
                                stfEmailIL.setError("Email is invalid");
                                stfEmailIL.requestFocus();
                            } catch (Exception e) {
                                Log.e("Login", e.getMessage());
                            }
                        }
                        hideProgressDialog();
                    }
                });


            }
        });


    }

}
