package com.vtcproject.mocha.teacher.ui.teacher;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.util.ArrayList;
import java.util.Random;


public class CreateRoom extends BaseFragment {
    private TeacherBaseViewModel mViewModel;
    private TextView roomNumTV;

    private FirebaseFirestore db;

    public CreateRoom() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teacher_create_room, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        roomNumTV = activity.findViewById(R.id.stfRoomNumTV);
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);

        activity.findViewById(R.id.stfCreateRoombtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.room.getRoomId() != null) {
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new ReadyRoom()).commitNow();
                } else {
                    Toast.makeText(activity, "Please wait room Number", Toast.LENGTH_SHORT).show();
                }

            }
        });
        createRoom();
    }

    private void createRoom() {
        db = FirebaseFirestore.getInstance();
        db.collection("Rooms").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                String roomNum = randomRoomNumber();
                if (queryDocumentSnapshots.size() > 0) {
                    roomNum = getUniqueRoom(queryDocumentSnapshots);
                }
                mViewModel.room = new Room();
                mViewModel.room.setRoomId(roomNum);
                mViewModel.room.setFixRoomId(roomNum);
                mViewModel.room.setRound(1);
                mViewModel.room.setTeacherId(mViewModel.teacher.getUid());
                String id = db.collection("Rooms").document().getId();
                String gameid = db.collection("Games").document().getId();
                mViewModel.room.setUid(id);
                mViewModel.room.setGameId(gameid);

                db.collection("Rooms").document(id).set(mViewModel.room);
                mViewModel.teacher.newGame(gameid);
                mViewModel.gameId = gameid;
                db.collection("Teachers").document(mViewModel.teacher.getUid()).set(mViewModel.teacher); //need change to transaction
                roomNumTV.setText(roomNum);
            }
        });
    }

    private String getUniqueRoom(QuerySnapshot queryDocumentSnapshots) {
        String tmpNum;
        while (true) {
            tmpNum = randomRoomNumber();
            ArrayList<String> roomList = new ArrayList<>();
            for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                roomList.add((String) doc.getData().get("roomId"));
            }
            if (!roomList.contains("tmpNum")) {
                break;
            }
        }
        return tmpNum;
    }

    private String randomRoomNumber() {
        return Integer.toString(new Random().nextInt(8999) + 1000);
    }
}
