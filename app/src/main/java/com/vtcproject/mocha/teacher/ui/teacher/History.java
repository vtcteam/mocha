package com.vtcproject.mocha.teacher.ui.teacher;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vtcproject.mocha.BaseFragment;
import com.vtcproject.mocha.R;
import com.vtcproject.mocha.models.Room;
import com.vtcproject.mocha.models.Student;
import com.vtcproject.mocha.teacher.TeacherBaseViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class History extends BaseFragment {
    private FirebaseFirestore db;
    private TeacherBaseViewModel mViewModel;
    private DocumentReference documentReference;
    private RecyclerView rvHistoryItems;
    private HistoryListAdapter historyListAdapter;

    public History(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_game_summary, container, false);
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        db = FirebaseFirestore.getInstance();
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());


        rvHistoryItems = activity.findViewById(R.id.rvHistoryItems);
        historyListAdapter = new HistoryListAdapter(activity, activity.getApplicationContext(), mViewModel.room.getStudents(), mViewModel.room);
        rvHistoryItems.setLayoutManager(new LinearLayoutManager(activity));
        rvHistoryItems.setAdapter(historyListAdapter);

    }


}

class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.HistoryViewHolder> {
    private FragmentActivity activity;
    private Context context;
    private ArrayList<Student> mData;
    private TeacherBaseViewModel mViewModel;
    private FirebaseFirestore db;
    private DocumentReference documentReference;
    private Room room;

    HistoryListAdapter(FragmentActivity activity, Context context, ArrayList<Student> mData, Room room) {
        this.activity = activity;
        this.context = context;
        this.mData = mData;
        this.room = room;
        mViewModel = ViewModelProviders.of(activity).get(TeacherBaseViewModel.class);
        db = FirebaseFirestore.getInstance();
        System.out.println(mViewModel.room.getUid());
        documentReference = db.collection("Rooms").document(mViewModel.room.getUid());
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_teacher_history_items, parent, false);
        HistoryViewHolder holder = new HistoryViewHolder(view);
        holder.tvHistoryDate = view.findViewById(R.id.tvHistoryDate);
        holder.tvHistoryStartTime = view.findViewById(R.id.tvHistoryStartTime);
        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull HistoryListAdapter.HistoryViewHolder holder, int position) {
        final Student student = mData.get(position);

    }


    @Override
    public int getItemCount() {
        if (room.getTeams() != null) {
            return room.getTeams().size();
        } else {
            return mData.size();
        }
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvHistoryDate, tvHistoryStartTime;
        HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
